﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Attack : MonoBehaviour {

    public GameObject MidPunch, LowPunch, MidGuard, LowGuard;

    public bool IsFighting = false;

    Player_Movement _playerMove;

    public bool Crouched;

    void Awake () {

        InactivePunchGuard();
        _playerMove = FindObjectOfType<Player_Movement>();


    }

    void FixedUpdate () {

        PlayerInput();
		
	}

    private void SetMidPunchOn()
    {
        if (MidPunch.activeSelf)
        {
            MidPunch.GetComponent<HitScript>().RayCastingP();
        }
    }
    private void SetLowPunchOn()
    {
        if (LowPunch.activeSelf)
        {
            LowPunch.GetComponent<HitScript>().RayCastingP();
        }
    }

    void PlayerInput()
    {
        Crouched = _playerMove.isCrouched;

        if (Input.GetKeyDown(KeyCode.Q) && IsFighting == false)
        {
            if (Crouched)
            {
                StartCoroutine("LowGuarding");
            }
            else if (!Crouched)
            {
                StartCoroutine("MidGuarding");
            }
        }

        if (Input.GetKeyDown(KeyCode.F) && IsFighting == false)
        {
            if (Crouched)
            {
                StartCoroutine("LowPunching");
            }
            else if (!Crouched)
            {
                StartCoroutine("MidPunching");
            }
        }

    }

    IEnumerator MidGuarding()
    {
        IsFighting = true;

        MidGuard.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        InactivePunchGuard();

        yield return new WaitForSeconds(0.1f);

        IsFighting = false;
    }

    IEnumerator LowGuarding()
    {
        IsFighting = true;

        LowGuard.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        InactivePunchGuard();

        yield return new WaitForSeconds(0.1f);

        IsFighting = false;
    }

    IEnumerator MidPunching()
    {
        IsFighting = true;

        MidPunch.SetActive(true);

        SetMidPunchOn();

        yield return new WaitForSeconds(0.5f);

        InactivePunchGuard();


        yield return new WaitForSeconds(0.1f);

        IsFighting = false;
    }

    IEnumerator LowPunching()
    {
        IsFighting = true;

        LowPunch.SetActive(true);

        SetLowPunchOn();

        yield return new WaitForSeconds(1.5f);

        InactivePunchGuard();

        yield return new WaitForSeconds(0.1f);

        IsFighting = false;
    }



    void InactivePunchGuard()
    {
        MidPunch.SetActive(false);
        LowPunch.SetActive(false);
        MidGuard.SetActive(false);
        LowGuard.SetActive(false);
    }

}
