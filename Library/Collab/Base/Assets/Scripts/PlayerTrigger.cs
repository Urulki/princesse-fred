﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrigger : MonoBehaviour {

    Camera_Movement cameraScript;

    void Awake()
    {
        cameraScript = FindObjectOfType<Camera_Movement>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "CameraMove")
        {
            cameraScript.MoveToNextStage();
        }
    }
}
