﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour {

    public float speed;

    public bool isCrouched = false;

    Collider  standColl, crouchColl;

    SpriteRenderer idleStand, idleCrouch;

    Player_Attack playerAttack;

    bool playerFighting;
    public bool faceRight;

    Rigidbody rb;

	void Awake () {

        idleStand = transform.Find("Shapes/Idle_Stand").GetComponent<SpriteRenderer>();
        idleCrouch = transform.Find("Shapes/Idle_Crouch").GetComponent<SpriteRenderer>();
        standColl = transform.Find("Stand_Hitbox").GetComponent<Collider>();
        crouchColl = transform.Find("Crouch_Hitbox").GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
        playerAttack = FindObjectOfType<Player_Attack>();
		
	}

    void Start()
    {
        standColl.enabled = true;
        crouchColl.enabled = false;
        idleStand.enabled = true;
        idleCrouch.enabled = false;
        faceRight = true;
    }

    void FixedUpdate () {

        PlayerInput();

        playerFighting = playerAttack.IsFighting;

        if (!isCrouched && !playerFighting)
        {
            Vector3 position = transform.position + Vector3.right * Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime;
            rb.MovePosition(position);

            if (Input.GetAxisRaw("Horizontal") > 0 && !faceRight)
            {
                Flip();
            }
            else if (Input.GetAxisRaw("Horizontal") < 0 && faceRight)
            {
                Flip ();
            }
        }
        

    }

    void Flip ()
    {
        faceRight = !faceRight;
        Vector2 scale = transform.localScale;
        scale *= -1;
        transform.localScale = scale;
    }

    void PlayerInput()
    {

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Debug.Log("Crouch");
            standColl.enabled = false;
            crouchColl.enabled = true;
            idleStand.enabled = false;
            idleCrouch.enabled = true;
            isCrouched = true;
        }

        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            Debug.Log("Uncrouch");
            standColl.enabled = true;
            crouchColl.enabled = false;
            idleStand.enabled = true;
            idleCrouch.enabled = false;
            isCrouched = false;
        }
    }
}
