﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitScript : MonoBehaviour {

    public LifeGestionScript LifeGestion;
    public Player_Movement Player_Movement;

	public void RayCastingE()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, transform.forward, Color.black);
            if (hit.collider.gameObject.tag != "Guard")
            {
                LifeGestion.HitDamage(gameObject, hit.collider.gameObject);
            }
            if (hit.collider.gameObject.tag == "Guard")
            {
                Debug.Log("yousk Blocked");
                    
            }
        }       
    }

    [ContextMenu("Launch")]
    public void RayCastingP()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.right, out hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, transform.right, Color.red, 2);
            if (hit.collider.gameObject.tag != "Guard")
            {
                LifeGestion.HitDamage(gameObject, hit.collider.gameObject);
                Debug.Log(hit.collider.gameObject);
            }
            if (hit.collider.gameObject.tag == "Guard")
            {
                Debug.Log("yousk Blocked");                
            }
        }
        
    }
}
