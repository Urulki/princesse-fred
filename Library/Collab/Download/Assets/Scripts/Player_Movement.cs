﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour {

    public float speed;

    public bool isCrouched, playerPunch, playerGuard, isWalking;

    public bool faceRight;

    Collider  standColl, crouchColl;

    SpriteRenderer idleStand, idleCrouch;

    Player_Attack playerAttack;


    Rigidbody rb;

	void Awake ()
    {

        standColl = transform.Find("Stand_Hitbox").GetComponent<Collider>();
        crouchColl = transform.Find("Crouch_Hitbox").GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
        playerAttack = FindObjectOfType<Player_Attack>();
		
	}

    void Start()
    {
        standColl.enabled = true;
        crouchColl.enabled = false;
        faceRight = true;
    }

    void FixedUpdate () {

        PlayerInput();

        playerPunch = playerAttack.isPunching;
        playerGuard = playerAttack.isGuarding;

        if (!isCrouched && !playerGuard && !playerPunch)
        {
            Vector3 position = transform.position + Vector3.right * Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime;
            rb.MovePosition(position);
            isWalking = true;

            if (Input.GetAxisRaw("Horizontal") > 0 && !faceRight)
            {
                Flip();
            }
            else if (Input.GetAxisRaw("Horizontal") < 0 && faceRight)
            {
                Flip();
            }
        }

        if (Input.GetAxisRaw("Horizontal") == 0)
        {
            isWalking = false;
        }
      

    }

    void Flip ()
    {
        isWalking = true;
        faceRight = !faceRight;
        Vector2 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    void PlayerInput()
    {

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Debug.Log("Crouch");
            standColl.enabled = false;
            crouchColl.enabled = true;
            isCrouched = true;
        }

        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            Debug.Log("Uncrouch");
            standColl.enabled = true;
            crouchColl.enabled = false;
            isCrouched = false;
        }
    }
}
