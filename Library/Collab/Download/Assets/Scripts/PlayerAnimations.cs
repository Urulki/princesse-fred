﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour {

    Animator anim;

    Player_Movement playerMove;
    Player_Attack playerAttack;

    public bool walking, crouching, punching, guarding;

    private void Awake()
    {
        playerMove = GetComponent<Player_Movement>();
        playerAttack = GetComponent<Player_Attack>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        walking = playerMove.isWalking;
        crouching = playerMove.isCrouched;
        punching = playerAttack.isPunching;
        guarding = playerAttack.isGuarding;
        SendAnimInfo();
    }

    void SendAnimInfo()
    {
        anim.SetBool("IsWalking", walking);
        anim.SetBool("IsCrouching", crouching);
        anim.SetBool("IsPunching", punching);
        anim.SetBool("IsGuarding", guarding);
    }
}
