﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Attack : MonoBehaviour {

    public GameObject MidPunch, LowPunch, MidGuard, LowGuard;

    public bool isCrouched, isPunching, isGuarding, isFighting  = false;

    Player_Movement _playerMove;

    public AudioClip punchHit1;
    public AudioClip punchHit2;


    void Awake () {

        InactivePunchGuard();
        _playerMove = FindObjectOfType<Player_Movement>();


    }

    void FixedUpdate () {

        PlayerInput();
		
	}

    private void SetMidPunchOn()
    {
        if (MidPunch.activeSelf)
        {
            MidPunch.GetComponent<HitScript>().RayCastingP();
        }
    }
    private void SetLowPunchOn()
    {
        if (LowPunch.activeSelf)
        {
            LowPunch.GetComponent<HitScript>().RayCastingP();
        }
    }

    void PlayerInput()
    {
        isCrouched = _playerMove.isCrouched;

        if (Input.GetButtonDown("Fire2") && !isPunching & !isGuarding)
        {
            if (isCrouched)
            {
                StartCoroutine("LowGuarding");
            }
            else if (!isCrouched)
            {
                StartCoroutine("MidGuarding");
            }
        }

        if (Input.GetButtonDown("Fire1") && !isPunching && !isGuarding)
        {
            if (isCrouched)
            {
                StartCoroutine("LowPunching");
            }
            else if (!isCrouched)
            {
                StartCoroutine("MidPunching");
            }
        }

    }

    IEnumerator MidGuarding()
    {
        isGuarding = true;

        MidGuard.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        InactivePunchGuard();

        yield return new WaitForSeconds(0.1f);

        isGuarding = false;
    }

    IEnumerator LowGuarding()
    {
        isGuarding = true;

        LowGuard.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        InactivePunchGuard();

        yield return new WaitForSeconds(0.1f);

        isGuarding = false;
    }

    IEnumerator MidPunching()
    {
        isPunching = true;

        MidPunch.SetActive(true);

        SetMidPunchOn();

        SoundManager.instance.RandomizeSfx(punchHit1, punchHit2);

        yield return new WaitForSeconds(0.5f);

        InactivePunchGuard();


        yield return new WaitForSeconds(0.1f);

        isPunching = false;
    }

    IEnumerator LowPunching()
    {
        isPunching = true;

        LowPunch.SetActive(true);

        SetLowPunchOn();

        SoundManager.instance.RandomizeSfx(punchHit1, punchHit2);

        yield return new WaitForSeconds(0.5f);

        InactivePunchGuard();

        yield return new WaitForSeconds(0.1f);

        isPunching = false;
    }



    void InactivePunchGuard()
    {
        MidPunch.SetActive(false);
        LowPunch.SetActive(false);
        MidGuard.SetActive(false);
        LowGuard.SetActive(false);
    }

}
