﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrigger : MonoBehaviour {

    public Camera_Movement cameraScript;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "CameraMove")
        {
            cameraScript.MoveToNextStage();
            Destroy(other.gameObject);
        }
    }
}
