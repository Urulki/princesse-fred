﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    Rigidbody playerRb;
    [Range(0f,10f)] public float speed;
	// Use this for initialization
	void Start () {
        playerRb = this.gameObject.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        Mouvement();

	}

    void Mouvement()
    {
        Vector3 targetPosition = transform.position + Vector3.right *Input.GetAxisRaw("Horizontal")* speed * Time.deltaTime;
        playerRb.MovePosition(targetPosition);
    }
}
