﻿using UnityEngine;

namespace Scriptables
{
	[CreateAssetMenu(fileName = "Stats",menuName = "Info/Stats")]
	public class Stats : ScriptableObject
	{

		public float Life;
		public float Damages;
		[HideInInspector]public float CurrentLife;

		void Start()
		{
			CurrentLife = Life;
		}
	}
}
