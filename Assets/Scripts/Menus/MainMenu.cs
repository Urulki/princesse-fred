﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {


    public Image cursor;
    public Sprite cursorPressed;

    public void Play()
    {
        StartCoroutine(NextAction(1));
    }

    public void Credits()
    {
        StartCoroutine(NextAction(2));
    }

    public void Quit()
    {
        Application.Quit();
    }


    IEnumerator NextAction(int indexToLoad)
    {
        cursor.sprite = cursorPressed;
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(indexToLoad);
    }
}
 