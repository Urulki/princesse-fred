﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CursorNavigation : MonoBehaviour {

    int index = 1;

    public int totalButtons;
    public float yOffset;

    private void Start()
    {
        
    }

    private void Update()
    {
        if (Input.GetAxis("Horizontal") < 0)
        {
            if (index < totalButtons)
            {
                index++;
                Vector2 position = transform.position;
                position.y -= yOffset;
                transform.position = position;
            }
        }

        if (Input.GetAxis("Horizontal") > 0)
        {
            if (index > 1)
            {
                index--;
                Vector2 position = transform.position;
                position.y += yOffset;
                transform.position = position;
            }
        }

        if (Input.GetButtonDown("Fire2"))
        {
            SceneManager.LoadScene(index);
        }
    }
}
