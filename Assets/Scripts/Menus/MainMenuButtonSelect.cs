﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuButtonSelect : MonoBehaviour, ISelectHandler
{

    public RectTransform cursor;

	public void OnSelect (BaseEventData eventData)
    {
        cursor.anchorMin = new Vector2(cursor.anchorMin.x, GetComponent<RectTransform>().anchorMin.y);
        cursor.anchorMax = new Vector2(cursor.anchorMax.x, GetComponent<RectTransform>().anchorMax.y);
    }
}
