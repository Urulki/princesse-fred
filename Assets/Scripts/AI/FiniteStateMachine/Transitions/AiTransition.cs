﻿using System;
using AI.FiniteStateMachine.Conditions;
using AI.FiniteStateMachine.States;

namespace AI.FiniteStateMachine.Transitions {
    [Serializable]
    public class AiTransition {

        public AbstractAiCondition Condition;
        public AiState TrueAiState;
        public AiState FalseAiState;
		
    }
}