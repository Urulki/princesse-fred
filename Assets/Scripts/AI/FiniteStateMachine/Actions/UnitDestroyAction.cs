﻿using UnityEngine;

namespace AI.FiniteStateMachine.Actions {
	[CreateAssetMenu(fileName = "UnitDestroyAction", menuName = "AI/FiniteStateMachine/Action/UnitDestroyAction")]
	public class UnitDestroyAction : AbstractAiAction {

		public override void Act(UnitAiComponent unitAiComponent) {
			Destroy(unitAiComponent.gameObject);
		}
		
	}
}
