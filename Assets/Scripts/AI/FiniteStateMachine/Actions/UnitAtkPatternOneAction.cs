﻿using System.Collections;
using System.Collections.Generic;
using AI.FiniteStateMachine.Actions;
using UnityEngine;

namespace AI.FiniteStateMachine.Actions
{
	[CreateAssetMenu(fileName = "UnitAtkPatternOneAction", menuName = "AI/FiniteStateMachine/Action/UnitAtkPatternOneAction")]
	public class UnitAtkPatternOneAction : AbstractAiAction
	{
		
		public override void Act(UnitAiComponent unitAiComponent)
		{
			unitAiComponent.navMeshObstacle.enabled = true;
			if (unitAiComponent.PatternOn) return;
			unitAiComponent.LaunchFirstEnnemyCoroutine(unitAiComponent);
		}
	}
}