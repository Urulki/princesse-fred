﻿using UnityEngine;

namespace AI.FiniteStateMachine.Actions {
	public abstract class AbstractAiAction : ScriptableObject {

		public abstract void Act(UnitAiComponent unitAiComponent);

	}
}
