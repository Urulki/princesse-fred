﻿using UnityEngine;

namespace AI.FiniteStateMachine.Actions
{
	[CreateAssetMenu(fileName = "MoveAction", menuName = "AI/FiniteStateMachine/Action/MoveAction")]
	public class MoveAction : AbstractAiAction
	{
		private float timer;
		public Sprite StepLeft;
		public Sprite StepRight;
		public override void Act(UnitAiComponent unitAiComponent)
		{
			unitAiComponent.navMeshObstacle.enabled = false;
			unitAiComponent.navMeshAgent.SetDestination(unitAiComponent.Target.position);
		}
	}
}
