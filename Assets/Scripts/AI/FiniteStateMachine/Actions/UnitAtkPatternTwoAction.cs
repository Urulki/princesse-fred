﻿using UnityEngine;

namespace AI.FiniteStateMachine.Actions
{
	[CreateAssetMenu(fileName = "UnitAtkPatternTwoAction", menuName = "AI/FiniteStateMachine/Action/UnitAtkPatternTwoAction")]
	public class UnitAtkPatternTwoAction : AbstractAiAction 
	{

		public override void Act(UnitAiComponent unitAiComponent)
		{
			unitAiComponent.navMeshObstacle.enabled = true;
			if (unitAiComponent.PatternOn) return;
			unitAiComponent.LaunchSecondEnnemyCoroutine(unitAiComponent);
		}
	}
}
