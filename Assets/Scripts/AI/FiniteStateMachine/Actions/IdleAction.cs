﻿using UnityEngine;

namespace AI.FiniteStateMachine.Actions
{
	[CreateAssetMenu(fileName = "Idle", menuName = "AI/FiniteStateMachine/Action/IdleAction")]
	public class IdleAction : AbstractAiAction
	{
		public Sprite TargetSprite;
		public override void Act(UnitAiComponent unitAiComponent)
		{
			unitAiComponent.navMeshObstacle.enabled = true;
			Debug.Log("IdleAct");
			unitAiComponent.sprite = TargetSprite;
		}
	}
}
