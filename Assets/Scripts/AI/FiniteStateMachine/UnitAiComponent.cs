﻿using System.Collections;
using System.Collections.Generic;
using AI.FiniteStateMachine.States;
using UnityEngine;
using UnityEngine.AI;

public class UnitAiComponent : MonoBehaviour
{

	public Sprite sprite;
	[HideInInspector]public NavMeshAgent navMeshAgent;
	[HideInInspector] public NavMeshObstacle navMeshObstacle;
	
	public bool PatternOn;
	public bool HasTargetOrder;
	public bool PatternFinished;

	[HideInInspector] public float YRot; 
	
	public AiState CurrentState;
	
	public Transform Target;
	
	public GameObject CrouchBox;
	public GameObject StandBox;
	public GameObject FullGuard;
	
	// Use this for initialization
	void Awake ()
	{
		navMeshObstacle = GetComponent<NavMeshObstacle>();
		navMeshAgent = GetComponent<NavMeshAgent>();
		CrouchBox = GameObject.FindGameObjectWithTag("CrouchBox");
		StandBox = GameObject.FindGameObjectWithTag("StandBox");
		FullGuard = GameObject.FindGameObjectWithTag("Guard");
		PatternFinished = false;
		PatternOn = false;
		FullGuard.SetActive(false);
		YRot = transform.localEulerAngles.y;
	}
	
	// Update is called once per frame
	void Update () {
		CurrentState.UpdateState(this);
		GetComponentInChildren<SpriteRenderer>().sprite = sprite;
	}
	
	public void TransitionToState(bool transitionSucced, AiState falseState, AiState trueState)
	{
		if (transitionSucced) CurrentState = trueState;
		else CurrentState = falseState;
	}

	public void LaunchFirstEnnemyCoroutine(UnitAiComponent unitAiComponent)
	{
		StartCoroutine(PatternOneCoroutine(unitAiComponent));
	}
	
	public void LaunchSecondEnnemyCoroutine(UnitAiComponent unitAiComponent)
	{
		StartCoroutine(PatternTwoCoroutine(unitAiComponent));
	}
	
	IEnumerator PatternOneCoroutine(UnitAiComponent unitAiComponent)
	{
		Debug.Log("PatternOne "+ gameObject.name);
		PatternOn = true;
		PatternFinished = false;
		yield return new WaitForSeconds(0.2f);
		unitAiComponent.GetComponentInChildren<HitScript>().RayCastingE();
		yield return new WaitForSeconds(1.2f);
		StandBox.SetActive(false);
		CrouchBox.SetActive(true);
		yield return new WaitForSeconds(0.2f);
		unitAiComponent.GetComponentInChildren<HitScript>().RayCastingE();
		yield return new WaitForSeconds(1.2f);
		CrouchBox.SetActive(false);
		StandBox.SetActive(true);
		yield return new WaitForSeconds(0.4f);
		PatternOn = false;
		PatternFinished = true;
		yield return null;
	}
	IEnumerator PatternTwoCoroutine(UnitAiComponent unitAiComponent)
	{
		Debug.Log("PatternTwo "+ gameObject.name);
		PatternOn = true;
		PatternFinished = false;
		yield return new WaitForSeconds(0.2f);
		unitAiComponent.GetComponentInChildren<HitScript>().RayCastingE();
		yield return new WaitForSeconds(.7f);
		FullGuard.SetActive(true);
		yield return new WaitForSeconds(0.5f);
		FullGuard.SetActive(false);
		yield return new WaitForSeconds(0.7f);
		unitAiComponent.GetComponentInChildren<HitScript>().RayCastingE();
		yield return new WaitForSeconds(1.2f);
		yield return new WaitForSeconds(0.4f);
		PatternOn = false;
		PatternFinished = true;
		yield return null;
	} 
}
