﻿using AI.FiniteStateMachine.Transitions;
using UnityEngine;
using AbstractAiAction = AI.FiniteStateMachine.Actions.AbstractAiAction;

namespace AI.FiniteStateMachine.States {
    [CreateAssetMenu(fileName = "New State", menuName = "AI/FiniteStateMachine/State")]
    public class AiState : ScriptableObject {

        public AbstractAiAction[] Actions;
        public AiTransition[] AiTransitions;

        public void UpdateState(UnitAiComponent unitAiComponent)
        {
            Debug.Log(name);
            DoActions(unitAiComponent);
            CheckTransitions(unitAiComponent);
        }
		
        private void DoActions(UnitAiComponent unitAiComponent) {
            foreach (AbstractAiAction action in Actions) {
                action.Act(unitAiComponent);
            }
        }

        private void CheckTransitions(UnitAiComponent unitAiComponent) {
            foreach (AiTransition transition in AiTransitions) {
                bool decisionSucceeded = transition.Condition.Check(unitAiComponent);
                unitAiComponent.TransitionToState(decisionSucceeded, transition.FalseAiState, transition.TrueAiState);
            }
        }

    }
}