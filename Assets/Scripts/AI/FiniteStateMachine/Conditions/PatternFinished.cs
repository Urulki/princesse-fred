﻿using System.Collections;
using System.Collections.Generic;
using AI.FiniteStateMachine.Conditions;
using UnityEngine;
[CreateAssetMenu(fileName = "Pattern Finished", menuName = "AI/FiniteStateMachine/Condition/PatternFinished")]
public class PatternFinished : AbstractAiCondition {
	public override bool Check(UnitAiComponent unitAiComponent)
	{
		if (!unitAiComponent.PatternFinished) return false;
		else return true;
	}
}
