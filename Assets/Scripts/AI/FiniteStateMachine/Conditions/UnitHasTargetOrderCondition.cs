﻿using UnityEngine;

namespace AI.FiniteStateMachine.Conditions {
	[CreateAssetMenu(fileName = "UnitHasTargetOrderCondition", menuName = "AI/FiniteStateMachine/Condition/UnitHasTargetOrderCondition")]
	public class UnitHasTargetOrderCondition : AbstractAiCondition {

		public override bool Check(UnitAiComponent unitAiComponent) {
			Debug.Log("je check");
			Debug.Log(unitAiComponent.name + " " + 
			          Vector3.Angle(new Vector3(0,0,unitAiComponent.transform.position.z),new Vector3(0,0,unitAiComponent.Target.position.z)));

			if (unitAiComponent.transform.position.x > unitAiComponent.Target.position.x) unitAiComponent.YRot = -90;
			if (unitAiComponent.transform.position.x < unitAiComponent.Target.position.x) unitAiComponent.YRot = 90;
			unitAiComponent.transform.localEulerAngles = new Vector3(0,unitAiComponent.YRot);
			if (Vector3.Distance(unitAiComponent.transform.position, unitAiComponent.Target.position)
			    >= unitAiComponent.navMeshAgent.stoppingDistance
					&& Vector3.Angle(new Vector3(0,0,unitAiComponent.transform.position.z),new Vector3(0,0,unitAiComponent.Target.position.z)) ==0f)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

	}
}
