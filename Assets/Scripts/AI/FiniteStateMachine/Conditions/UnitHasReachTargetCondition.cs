﻿using UnityEngine;

namespace AI.FiniteStateMachine.Conditions {
	[CreateAssetMenu(fileName = "UnitHasReachTargetCondition", menuName = "AI/FiniteStateMachine/Condition/UnitHasReachTargetCondition")]
	public class UnitHasReachTargetCondition : AbstractAiCondition {

		public override bool Check(UnitAiComponent unitAiComponent) {
			if (Vector3.Distance(unitAiComponent.transform.position, unitAiComponent.Target.position)
			    <= unitAiComponent.navMeshAgent.stoppingDistance)
			{
				//Debug.Log("Je suis proche" + (unitAiComponent.navMeshAgent.remainingDistance >= unitAiComponent.navMeshAgent.stoppingDistance + 1));
				return true;
			}

			else
			{
				//Debug.Log(unitAiComponent.navMeshAgent.remainingDistance + "\n" + unitAiComponent.navMeshAgent.stoppingDistance);
				return false;
			}
		}
		
	}
}
