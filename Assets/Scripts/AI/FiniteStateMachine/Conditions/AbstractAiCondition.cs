﻿using UnityEngine;

namespace AI.FiniteStateMachine.Conditions {
	public abstract class AbstractAiCondition : ScriptableObject {

		public abstract bool Check(UnitAiComponent unitAiComponent);
		
	}
}
