﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIScript : MonoBehaviour {

    Transform target;

    NavMeshAgent enemyAgent;

    public GameObject CrouchCollider;
    public GameObject StandUpCollider;
    public GameObject MidGuard;
    public GameObject LowGuard;

    GameObject _midPunch;
    GameObject _lowPunch;

    public int coolDown;

    public bool IsAttacking;

    bool _isCrouch;
    bool _isBlocking;

    void Start ()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        enemyAgent = GetComponent<NavMeshAgent>();
        _midPunch = GameObject.Find("MidPunchRay");
        _lowPunch = GameObject.Find("LowPunchRay");
        _isCrouch = false;
        _isBlocking = false;
        MidGuard.SetActive(false);
        LowGuard.SetActive(false);
    }

    void Update () {
        enemyAgent.SetDestination(target.position);
        //Debug.Log(target.position);
        AICrouch();


        if (_isCrouch)
        {
            CrouchCollider.SetActive(true);
            StandUpCollider.SetActive(false);
        }
        else
        {
            CrouchCollider.SetActive(false);
            StandUpCollider.SetActive(true);
        }
        AIBlock();
        AILowBlock();
        AIMidBlock();
        if (!_isBlocking)
        {
            MidGuard.SetActive(false);
            LowGuard.SetActive(false);
        }
    }
    void AIMidAttack()
    {
        if (IsAttacking)
        {
            if (transform.position.x - target.position.x <= 2.1f && transform.position.x - target.position.x >= 1.9f && IsAttacking)
            {
                _midPunch.GetComponent<HitScript>().RayCastingE();
            }
        }
    }
    void AILowAttack()
    {
        if (IsAttacking)
        {
            if (transform.position.x - target.position.x <= 2.1f && transform.position.x - target.position.x >= 1.9f && IsAttacking)
            {
                _lowPunch.GetComponent<HitScript>().RayCastingE();
            }
        }
    }
    void AIMidBlock()
    {
        if (_isBlocking && !_isCrouch)
        {
            MidGuard.SetActive(true);
            Debug.Log("Issou je bloque mid");
            LowGuard.SetActive(false);
        }
    }
    void AILowBlock()
    {
        if (_isBlocking && _isCrouch)
        {
            LowGuard.SetActive(true);
            Debug.Log("Issou je bloque low");
            MidGuard.SetActive(false);
        }
    }

    void AICrouch()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Issou je marche");
            _isCrouch = true;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            Debug.Log("Issou je marche plus");
            _isCrouch = false;
        }
    }

    void AIBlock()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            _isBlocking = true;
            Debug.Log("Issou je marche");
        }
        if (Input.GetKeyUp(KeyCode.T))
        {
            _isBlocking = false;
        }
    }
}
