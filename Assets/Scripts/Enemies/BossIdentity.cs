﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossIdentity : MonoBehaviour
{
    public int HP;
    public float rate;
    public int patternCase;
    [Range(1,15)] public int patternLenght;

    public Sprite missilA, missilB, missilC;

    public GameObject projectileA, projectileB, mouthUp, mouthDown;

    bool isFiring;

    private void Start()
    {
    }

    private void Update()
    {
        if (isFiring) return;
        switch (patternCase)
        {
            case 0:
                //Debug.Log("Case Up");
                StartCoroutine(LaunchDestructUpMissile());
                break;
            case 1:
                //Debug.Log("Case Down");
                StartCoroutine(LaunchDestructDownMissile());
                break;
            case 2:
                //Debug.Log("Case No Destruct");
                StartCoroutine(LaunchUpMissile());
                break;
            default:
                ChangeState();
                break;
        }
    }

    IEnumerator LaunchDestructUpMissile()
    {
        //Debug.Log("LaunchMissileA");
        isFiring = true;
        InvokeRepeating("MissileUpDestruct", rate, rate);
        yield return new WaitForSeconds(patternLenght);
        CancelInvoke();
        yield return new WaitForSeconds(0.5f);
        ChangeState();
    }

    IEnumerator LaunchDestructDownMissile()
    {
        //Debug.Log("LaunchMissileB");
        isFiring = true;
        InvokeRepeating("MissileDownDestruct", rate, rate);
        yield return new WaitForSeconds(patternLenght);
        CancelInvoke();
        yield return new WaitForSeconds(0.5f);
        ChangeState();
    }

    IEnumerator LaunchUpMissile()
    {
        //Debug.Log("LauncheMissileC");
        isFiring = true;
        InvokeRepeating("MissileDownNoDestruct", rate, rate);
        yield return new WaitForSeconds(patternLenght);
        CancelInvoke();
        yield return new WaitForSeconds(0.5f);
        ChangeState();
    }

    void MissileUpDestruct()
    {
        //Debug.Log("MissileA");
        GameObject missile = Instantiate(projectileA, mouthUp.transform.position, Quaternion.identity);
        missile.GetComponentInChildren<SpriteRenderer>().sprite = missilA;
        
    }

    void MissileDownDestruct()
    {
        //Debug.Log("MissileB");
        GameObject missile = Instantiate(projectileA, mouthDown.transform.position, Quaternion.identity);
        missile.GetComponentInChildren<SpriteRenderer>().sprite = missilB;

    }

    void MissileDownNoDestruct()
    {
        //Debug.Log("MissileC");
        GameObject missile = Instantiate(projectileB, mouthUp.transform.position, Quaternion.identity);
        missile.GetComponentInChildren<SpriteRenderer>().sprite = missilC;

    }

    void ChangeState()
    {
        //Debug.Log("ChangeState");
        isFiring = false;
        patternCase = Random.Range(0, 3);
    }
}
