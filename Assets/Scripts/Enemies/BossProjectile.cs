﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossProjectile : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        Debug.Log(other.collider.tag);
        if (other.collider.tag == "Guard")
        {
            Debug.Log("BossProjectileHit");
            other.transform.Translate(-0.2f, 0, 0);
            Destroy(gameObject);
        }
    }
}
