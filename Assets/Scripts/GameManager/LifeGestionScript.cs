﻿using System.Collections;
using System.Collections.Generic;
using Scriptables;
using UnityEngine;
using UnityEngine.Serialization;

public class LifeGestionScript : MonoBehaviour
{

    public Stats Stats;
    public float CurrentLife;
    public float Damage;
    
    private void Awake()
    {
        CurrentLife = Stats.Life;
        Damage = Stats.Damages;
    }

    void FixedUpdate()
    {
        if (CurrentLife < 1)
        {
            Destroy(gameObject);
        } 
    }

    public void HitDamage(GameObject hitting, GameObject hitted)
    {
       
        Debug.Log("hitted");
        
        if (hitted.tag == "Enemy")
        {
            Debug.Log("yousk enemy");
            
        }
        if (hitted.tag == "Player")
        {
            Debug.Log("yourlysk");
        }
        hitted.GetComponentInParent<LifeGestionScript>().CurrentLife
            -= hitting.GetComponentInParent<LifeGestionScript>().Damage;
    }

}
