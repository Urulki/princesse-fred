﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Camera_Movement : MonoBehaviour {

    //public Transform target;
    public float smoothTime = 0.3F;
    public float cameraSpeed;

    //Vector3 velocity = Vector3.zero;

    Vector3 endPosition;
    //Vector3 offset = new Vector3(0f, 0f, -10f);

   public bool stageOn = false;

    void FixedUpdate()
    {
        //Debug.Log(Time.fixedDeltaTime);
        /*if (stageOn == true)
        {
            Vector3 targetPosition = target.transform.position + offset;
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        }*/       
    }

    public void MoveToNextStage()
    {
        Debug.Log("MoveNextStage");
        stageOn = false;
        transform.DOMoveX(transform.position.x + 10f, 5f, false).SetEase(Ease.Linear).OnComplete(StageOnOff);
    }

    void StageOnOff()
    {
        stageOn = !stageOn;

    }
}
