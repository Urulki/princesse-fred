﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSpawner_Script : MonoBehaviour {

    public Camera_Movement cameraScript;
    public List<GameObject> Spawners;

    private void Awake()
    {
        foreach (GameObject Spawn in Spawners)
        {
            Spawn.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            cameraScript.MoveToNextStage();
            foreach (GameObject Spawn in Spawners)
            {
                Spawn.SetActive(true);
            }
            Destroy(gameObject);
        }
    }
}
