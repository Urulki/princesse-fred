﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitScript : MonoBehaviour {

    public LifeGestionScript LifeGestion;

	public void RayCastingE()
    {
        Debug.Log("hitLaunch");
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 1.25f,9))
        {
            Debug.DrawRay(transform.position, transform.forward, Color.red);
            Debug.Log(hit.collider.gameObject);
            if (hit.collider.gameObject.tag != "Guard")
            {
                LifeGestion.HitDamage(gameObject, hit.collider.gameObject);
            }
            if (hit.collider.gameObject.tag == "Guard")
            {
                Debug.Log("yousk Blocked");    
            }
        }       
    }

    [ContextMenu("Launch")]
    public void RayCastingP()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.right, out hit, 1.25f,0,QueryTriggerInteraction.Ignore))
        {
            Debug.DrawRay(transform.position, transform.right, Color.blue);
            Debug.Log(hit.collider.gameObject);
            if (hit.collider.gameObject.tag != "Guard")
            {
                LifeGestion.HitDamage(gameObject, hit.collider.gameObject);
                Debug.Log(hit.collider.gameObject);
            }
            if (hit.collider.gameObject.tag == "Guard")
            {
                Debug.Log("yousk Blocked");                
            }
        }
        
    }
}
