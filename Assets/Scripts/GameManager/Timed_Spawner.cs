﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timed_Spawner : MonoBehaviour {

    public List<GameObject> spawnees = new List<GameObject>();
    public float timer;
    public float frequency;
    public float smoothedFrequency;


    void Start()
    {
        StartCoroutine(Spawning());
    }

    IEnumerator Spawning()
    {
        yield return new WaitForSeconds(timer);
        foreach (GameObject spawnedObj in spawnees)
        {
            Debug.Log("Enemy_Spawn");
            yield return new WaitForSeconds(frequency + Random.Range(0f, smoothedFrequency));
            Instantiate(spawnedObj, transform.transform.position, transform.rotation);
        }

        Destroy(gameObject);
    }
}
